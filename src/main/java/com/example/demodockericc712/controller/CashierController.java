package com.example.demodockericc712.controller;

import com.example.demodockericc712.model.Cashier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

// creo que esto de importar una variable estática pública desde otro paquete debe ser horriblemente mala práctica,
// pero bueno, al menos es estático y solo ocupa un espacio de memoria (o ese es el objetivo)
import static com.example.demodockericc712.dao.DataSource.cashierList;

@RestController
@RequestMapping("cashier")
public class CashierController {

    @GetMapping
    private List<Cashier> getCashierList() {
        return cashierList;
    }

    @GetMapping("{id}")
    private ResponseEntity<Cashier> getCashier(@PathVariable int id) {
        if (id >= cashierList.size())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(cashierList.get(id), HttpStatus.OK);
    }

}
