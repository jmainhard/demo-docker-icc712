package com.example.demodockericc712.controller;

import com.example.demodockericc712.model.Product;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.example.demodockericc712.dao.DataSource.productList;

@RestController
@RequestMapping("product")
public class ProductController {
    @GetMapping
    public List<Product> getProductList() {
        return productList;
    }

    @GetMapping("{id}")
    public ResponseEntity<Product> getProduct(@PathVariable int id) {
        if (id >= productList.size())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(productList.get(id), HttpStatus.OK);
    }
}
