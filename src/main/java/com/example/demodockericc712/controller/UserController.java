package com.example.demodockericc712.controller;

import com.example.demodockericc712.model.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.example.demodockericc712.dao.DataSource.userList;

@RestController
@RequestMapping("user")
public class UserController {
    @GetMapping
    private List<User> getUserList() {
        return userList;
    }

    @GetMapping("{id}")
    private ResponseEntity<User> getUser(@PathVariable int id) {
        if (id >= userList.size())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(userList.get(id), HttpStatus.OK);
    }
}
