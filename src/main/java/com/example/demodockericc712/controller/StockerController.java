package com.example.demodockericc712.controller;

import com.example.demodockericc712.model.Stocker;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.example.demodockericc712.dao.DataSource.stockerList;

@RestController
@RequestMapping("stocker")
public class StockerController {
    @GetMapping
    private List<Stocker> getStockerList() {
        return stockerList;
    }

    @GetMapping("{id}")
    private ResponseEntity<Stocker> getStocker(@PathVariable int id) {
        if (id >= stockerList.size())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(stockerList.get(id), HttpStatus.OK);
    }
}
