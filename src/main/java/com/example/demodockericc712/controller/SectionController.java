package com.example.demodockericc712.controller;

import com.example.demodockericc712.model.Section;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.example.demodockericc712.dao.DataSource.sectionList;

@RestController
@RequestMapping("section")
public class SectionController {
    @GetMapping
    private List<Section> getSectionList() {
        return sectionList;
    }

    @GetMapping("{section}")
    private ResponseEntity<Section> getSection(@PathVariable("section") String section) {
        try {
            return new ResponseEntity<>(Section.valueOf(section), HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
