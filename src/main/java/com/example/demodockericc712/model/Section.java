package com.example.demodockericc712.model;

public enum Section {
    PASILLO_1,
    PASILLO_2,
    PASILLO_3,
    UNDEFINED_SECTION;
}
