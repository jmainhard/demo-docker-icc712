package com.example.demodockericc712.model;

public class Cashier extends User {
    private Long ventas;

    public Cashier() {}

    public Cashier(User user, Long ventas) {
        super(user.getName(), user.getRut(), user.getUser(), user.getPassword());
        this.ventas = ventas;
    }

    public Cashier(String name, String rut, String user, String password, Long ventas) {
        super(name, rut, user, password);
        this.ventas = ventas;
    }

    public Long getVentas() {
        return ventas;
    }

    public void setVentas(Long ventas) {
        this.ventas = ventas;
    }
}
