package com.example.demodockericc712.model;

public class Product {
    private String name;
    private Section section;
    private int stock;
    private int price;

    public Product() {}

    public Product(String name, Section section, int stock, int price) {
        this.name = name;
        this.section = section;
        this.stock = stock;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
