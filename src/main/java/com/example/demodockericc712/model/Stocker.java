package com.example.demodockericc712.model;

public class Stocker extends User {
    private Section section;

    public Stocker() {}

    public Stocker(User user, Section section) {
        super(user.getName(), user.getRut(), user.getUser(), user.getPassword());
        this.section = section;
    }

    public Stocker(String name, String rut, String user, String password, Section section) {
        super(name, rut, user, password);
        this.section = section;
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }
}
