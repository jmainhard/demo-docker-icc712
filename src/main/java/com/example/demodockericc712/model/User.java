package com.example.demodockericc712.model;

public class User {
    private String name;
    private String rut;
    private String user;
    private String password;

    public User() {}
    public User(String name, String rut, String user, String password) {
        this.name = name;
        this.rut = rut;
        this.user = user;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
