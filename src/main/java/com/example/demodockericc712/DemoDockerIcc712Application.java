package com.example.demodockericc712;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoDockerIcc712Application {

	public static void main(String[] args) {
		SpringApplication.run(DemoDockerIcc712Application.class, args);
	}

}
