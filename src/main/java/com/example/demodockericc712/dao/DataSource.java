package com.example.demodockericc712.dao;

import com.example.demodockericc712.model.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.example.demodockericc712.util.Util.random;
import static com.example.demodockericc712.util.Util.randomInvalidRut;

public class DataSource {
    // FIXME: declaración de campos estáticos (cashierList y stockerList) dependen de Línea 16
    public static List<Cashier> cashierList = cashierList();
    public static List<Stocker> stockerList = stockerList();
    public static List<User> userList = userList();
    public static List<Product> productList = productList();
    public static List<Section> sectionList = Arrays.asList(Section.values());
    private static int index = 0;

    private static List<Product> productList() {
        String UNDEFINED = "UNDEFINED_PRODUCT_";
        return Arrays.asList(
                new Product(UNDEFINED + "1", Section.PASILLO_1, random(), random()),
                new Product(UNDEFINED + "2", Section.PASILLO_2, random(), random()),
                new Product(UNDEFINED + "3", Section.PASILLO_3, random(), random())
        );
    }

    private static List<User> userProvider() {
        String UNDEFINED_NAME = "UNDEFINED_NAME_";
        String UNDEFINED_USER = "UNDEFINED_USERNAME_";
        // FIXME: creo que hacer los nombres undefined aún asi personalizados le hace perder un poco el propósito
        //  de usuarios indefinidos (por index)
        return Arrays.asList(
                new User(UNDEFINED_NAME + ++index, randomInvalidRut(), UNDEFINED_USER + index, "123"),
                new User(UNDEFINED_NAME + ++index, randomInvalidRut(), UNDEFINED_USER + index, "123"),
                new User(UNDEFINED_NAME + ++index, randomInvalidRut(), UNDEFINED_USER + index, "123")
        );
    }

    private static List<Cashier> cashierList() {
        return userProvider()
                .stream()
                .map(user -> new Cashier(user, Integer.toUnsignedLong(random())))
                .toList();
    }

    private static List<Stocker> stockerList() {
        return userProvider()
                .stream()
                .map(user -> new Stocker(user, Section.UNDEFINED_SECTION))
                .toList();
    }

    private static List<User> userList() {
        List<User> allUsers = new ArrayList<>();
        allUsers.addAll(cashierList);
        allUsers.addAll(stockerList);
        return allUsers;
    }
}
