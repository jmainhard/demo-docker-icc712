FROM openjdk:17

EXPOSE 8080

COPY target/*.jar demo-api.jar

ENTRYPOINT ["java", "-jar", "demo-api.jar"]
